from z3 import *
from collections import defaultdict
from pprint import pprint

'''
class Address:
	def __init__(self, param1):
		self.address = self.address(param1)

	def toHex(self, dec):
		x = (dec % 16)
		digits = "0123456789ABCDEF"
		rest = dec // 16
		if (rest == 0):
		    return digits[x]
		return self.toHex(rest) + digits[x]

	def address(self, hex):
		hex_val = self.toHex(hex)
		length = 42 - len(hex_val)
		addr = '0x' + '0' * length + hex_val
		return addr 
'''

class Var:
	def __init__(self, val):
		self.before = val
		self.after = None

	def set_after(self, val):
		self.after = val

def _assert(res):
	global _assertion
	_assertion = And(_assertion, res)

def display(s):
	print("\n条件: ")
	pprint(s.assertions())
	print("\n结果: ")
	res = s.check()
	print(res)
	print("\n解: ")
	if res.r == 1:
		pprint(s.model())
	else:
		print("无解")

def get_result(s, cond):
	#print("\n条件: ")
	#pprint(s.assertions())

	print("\n检查: ")
	pprint(cond)

	print("\n结果: ")
	res = s.check(cond)
	print(res)

	print("\n解: ")
	if res.r == 1:
		pprint(s.model())
	else:
		print("无解")

s = Solver()

# functions to check
'''
    function approve(address spender, uint amount) public returns (bool) {
        _approve(_msgSender(), spender, amount);
        return true;
    }

    function _transfer(address sender, address recipient, uint amount) internal {
        require(sender != address(0), "ERC20: transfer from the zero address");
        require(recipient != address(0), "ERC20: transfer to the zero address");

        _balances[sender] = _balances[sender].sub(amount, "ERC20: transfer amount exceeds balance");
        _balances[recipient] = _balances[recipient].add(amount);
        emit Transfer(sender, recipient, amount);
    }

    function transferFrom(address sender, address recipient, uint amount) public returns (bool) {
        _transfer(sender, recipient, amount);
        _approve(sender, _msgSender(), _allowances[sender][_msgSender()].sub(amount, "ERC20: transfer amount exceeds allowance"));
        return true;
    }

'''
def _approve(owner, spender, value):
	_assert(owner != String('addr0'))
	_assert(spender != String("addr0"))

	global _allowances
	
	_allowances[owner][spender] = value

def _transfer(sender, recipient, amount):
	_assert(sender != String('addr0'))
	_assert(recipient != String('addr0'))
	_assert(sender != recipient)

	global _msgSender
	if sender == _msgSender:
		_balances[sender] = _balances[sender] + amount
		_balances[recipient] = _balances[recipient] - amount
	else:
		_balances[sender] = _balances[sender] - amount
		_balances[recipient] = _balances[recipient] + amount

def transferFrom(sender, recipient, amount):
	_transfer(sender, recipient, amount)
	_approve(sender, _msgSender, _allowances[sender][_msgSender] - amount)

# parameter variables
_msgSender = String('_msgSender')
sender = String("sender")
recipient = String("recipient")
amount = Real("amount")

# global variables
# mapping (address => mapping (address => uint256)) private _allowances;
_allowances = defaultdict(lambda: defaultdict(lambda: 0))
# mapping (address => uint) private _balances;
_balances = defaultdict(lambda: 0)
_assertion = Bool('_assertion')


# initial states
balance_sender = Var(_balances[sender])
balance_recipient = Var(_balances[recipient])
allowance_msg = Var(_allowances[sender][_msgSender])

# execution
transferFrom(sender, recipient, amount)

# result states
balance_sender.set_after(_balances[sender])
balance_recipient.set_after(_balances[recipient])
allowance_msg.set_after(_allowances[sender][_msgSender])

# prelimary condition
s.add(_assertion == True)
s.add(_msgSender == sender)
s.push()

'''
print("正例: ")

# expected outcome
s.add(balance_recipient.before + amount == balance_recipient.after)
s.add(balance_sender.before - amount == balance_sender.after)

s.add(allowance_msg.before - amount == allowance_msg.after)
display(s)

print('\n\n========================\n\n')
print("反例: ")

s.pop()
'''

# opposite of expect outcomes
'''
s.add(Or(Or(
	Not(balance_recipient.before + amount == balance_recipient.after), 
	Not(balance_sender.before - amount == balance_sender.after)), 
	Not(allowance_msg.before - amount == allowance_msg.after)))

display(s)
'''
#get_result(s, Not(balance_recipient.before + amount == balance_recipient.after))
get_result(s, Or(Or(
	Not(balance_recipient.before + amount == balance_recipient.after), 
	Not(balance_sender.before - amount == balance_sender.after)), 
	Not(allowance_msg.before - amount == allowance_msg.after)))
